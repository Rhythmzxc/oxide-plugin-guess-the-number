# Guess The Number #

### Current Version: 1.2.1 ###

### What is this for? ###
1. This is for [Rust](https://playrust.com/) | [Oxide Modded Server](http://oxidemod.org/)

### Direct Link to Plugin Download ###
* [Guess The Number](http://oxidemod.org/plugins/guess-the-number.2023/)

### Features ###
1. Auto-starting/ending of Event
1. Experiences as reward
1. Support for Economics Reward
1. Support for ServerRewards Reward
1. Maximum Attempts

### Chat Commands ###
1. /startNumber - Start the event. (Based on config min/maxNumber)
1. /startNumber <minNumber> <maxNumber> - Start the event with your desired random rolls.
1. /endNumber - Forcefully ending the event with no winner.
1. /number <number> - To reply your answer.

### Rewards ###
* (Optional) Experience
* (Optional) Economics $
* (Optional) ServerRewards Points

### Configurations ###

```
#!JSON

{
  "autoEndEventEnabler": true,
  "autoEndEventTimer": 300,
  "autoEventEnabler": true,
  "autoEventInterval": 1800,
  "economicsEnabler": false,
  "economicsWinReward": 100,
  "maxAttempts": 10,
  "maxAttemptsEnabler": false,
  "maxNumber": 101,
  "minNumber": 1,
  "serverRewardsEnabler": false,
  "serverRewardsPoints": 5,
  "xpEnabler": true,
  "xpPercentEnabler": false,
  "xpPercentToGive": 0.1,
  "xpToGive": 10
}
```

### Localizations ###

```
#!JSON

{
  "pluginPrefix": "<color=orange>Guess The Number</color>",
  "wrongNumber": "Ops, that is not the correct number!",
  "invalidParam": "Invalid Parameter - /number <number>",
  "invalidParam2": "Invalid Parameters - /startNumber <minNumber> <maxNumber>",
  "noPermission": "You do not have the permission to use this.",
  "winNumber": "Winning number: <color=orange>{0}</color>",
  "numberNotice": "Event has started, guess a number! ({0} - {1}). Reply using /number <number>",
  "maxAttempts": "You have reached the maximum attempts to guess a number.",
  "autoEventEnd": "Event has auto-ended due to time limit. The winning number was <color=orange>{0}</color>.",
  "eventWon": "<color=orange>{0}</color> won!\nThe winning number was <color=orange>{1}</color>.",
  "eventNotStarted": "Event has not started.",
  "eventForcedEnd": "Event has been ended by <color=orange>{0}</color>. The winning number was <color=orange>{1}</color>.",
  "eventStarted": "Someone has already started the event.\nWinning number: <color=orange>{0}</color>\n/endNumber - To end the event forcefully.",
  "eventWonEconomics": "\n<color=orange>${0}</color> to has been added to his balance.",
  "eventWonServerRewards": "\n<color=orange>{0}</color> server points has been added.",
  "eventWonExperiences": "\n<color=orange>{0}</color> experiences has been awarded."
}
```

### Permissions ###
* GuessTheNumber.startEvent - Allows user to start/end the event.

**To grant permissions, go to the Console and use this following commands:**

* grant user <name> GuessTheNumber.startEvent - Give permission to the player.
* grant group <name> GuessTheNumber.startEvent - Give permission to the group.

Players with authLevel > 1, do not have to be granted with permission to use all the commands.